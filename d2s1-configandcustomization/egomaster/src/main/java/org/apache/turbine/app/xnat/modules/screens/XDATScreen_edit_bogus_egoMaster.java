/*
 *
 */
package org.apache.turbine.app.xnat.modules.screens;

import org.apache.turbine.util.RunData;
import org.nrg.xdat.model.XnatSubjectassessordataI;
import org.nrg.xdat.om.BogusEgomaster;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFTItem;

import java.util.Date;
import java.util.List;

/**
 * @author XDAT
 *
 */
public class XDATScreen_edit_bogus_egoMaster extends org.nrg.xdat.turbine.modules.screens.XDATScreen_edit_bogus_egoMaster {
	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XDATScreen_edit_bogus_egoMaster.class);

    @Override
    public ItemI getEmptyItem(RunData data) throws Exception
    {
        final XDATUser user = TurbineUtils.getUser(data);
        final String projectId = data.getParameters().get("project");
        final String subjectId = data.getParameters().get("part_id");
        final BogusEgomaster egomaster = new BogusEgomaster(XFTItem.NewItem(getElementName(), user));
        egomaster.setId(XnatExperimentdata.CreateNewID());
        egomaster.setDate(new Date());
        egomaster.setProject(projectId);
        egomaster.setSubjectId(subjectId);
        egomaster.setLabel(getLabel(projectId, subjectId, user));
        if (logger.isDebugEnabled()) {
            logger.debug("Creating empty BogusEgoMaster assessment: " + egomaster.getLabel());
        }
        return egomaster.getItem();
    }

    private static String getLabel(String projectId, String subjectId, XDATUser user) {
        final XnatSubjectdata subject = XnatSubjectdata.getXnatSubjectdatasById(subjectId, user, true);
        List<XnatSubjectassessordataI> experiments = subject.getExperiments_experiment();
        int count = 0;
        for (XnatSubjectassessordataI experiment : experiments) {
            if (projectId.equals(experiment.getProject())) {
                count++;
            }
        }
        return String.format("%s-%03d", subject.getLabel(), count);
    }
}
