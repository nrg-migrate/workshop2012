/**
 * EgoMasterRestlet
 * (C) 2012 Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 *
 * Created on 6/20/12 by rherri01
 */
package org.nrg.xnat.restlet.extensions;

import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

@XnatRestlet("/services/extensions/egomaster/{ID}")
public class EgoMasterRestlet extends SecureResource {

    public EgoMasterRestlet(Context context, Request request, Response response) {
        super(context, request, response);
        getVariants().add(new Variant(MediaType.APPLICATION_JSON));
        _id = (String) getRequest().getAttributes().get("ID");
    }

    @Override
    public Representation represent() throws ResourceException {
        return new StringRepresentation("Hi there.");
    }

    final private String _id;
}
