#* @vtlvariable name="project" type="java.lang.String" *#
#* @vtlvariable name="link" type="org.apache.turbine.services.pull.tools.TemplateLink" *#
#* @vtlvariable name="ui" type="org.apache.turbine.services.pull.util.UIManager" *#
#* @vtlvariable name="data" type="org.apache.turbine.services.rundata.DefaultTurbineRunData" *#
#* @vtlvariable name="page" type="org.apache.turbine.util.template.HtmlPageAttributes" *#
#* @vtlvariable name="vr" type="org.nrg.xft.utils.ValidationUtils.ValidationResults" *#
#macro (showBogusItems $property $item $vr $array)
    #showBogusItemsAndLabels($property $item $vr $array $array)
#end
#macro (showBogusItemsAndLabels $property $item $vr $array $labels)
    #if ($vr)
        #if($vr.getField($property))
        <span style="color: red; ">&#8658</span>
        #end
    #end
    #set($value = $!item.getProperty($property))
<select id="$property" name="$property">
    <option value="" #if ( "" == $value) selected="true" #end>(SELECT)</option>
    #set($index = 0)
    #foreach($arrayItem in $array)
        #set($label = $labels.get($index))
        #set($index = $index + 1)
        <option value="$arrayItem" #if ($arrayItem == $value) selected="true" #end>$label</option>
    #end
</select>
#end
$page.setTitle("XDAT")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)
#if ($data.message)
<span style="color: red; font-size: small; ">$data.message</span>
#end
<p>
<form id="egoMasterForm" name="egoMasterForm" method="post" action="$link.setAction("ModifySubjectAssessorData")">
    <input type="hidden" name="project" value="$!{project}" >
    #if($vr)
        <span style="color: red; ">Invalid parameters:<BR>$vr.toHTML()</span>
        <hr>
    #end

    <table width="100%">
        <tr>
            <td style="vertical-align: top;">
                <table width="100%">
                    <tr>
                        <td align="left" valign="middle">
                            <div class="edit_title">EgoMaster Details</div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top;">
                <table width="100%">
                    <tr>
                        <td valign="top">
                            <table>
                                <tr><td colspan='2'>
                                    #parse("/screens/xnat_edit_subjectAssessorData.vm")
                                </td></tr>
                                <tr>
                                    <td colspan="2">#parse("/screens/EditProjectSpecificFields.vm")</td>
                                </tr>
                                <tr><th width="40%" style="text-align: left; vertical-align: top; font-weight: bold !important;">Date:</th><td style="vertical-align: top;">#xdatCalendar("bogus:egoMaster/date" $item $vr)</td></tr>
                                <tr><th style="text-align: left; vertical-align: top; font-weight: bold !important;">Is the subject awesome?</th><td style="vertical-align: top;">#xdatBooleanRadioYesNoWithoutDefault("bogus:egoMaster/awesome" $item $vr)</td></tr>
                                <tr>
                                    <th style="text-align: left; vertical-align: top; font-weight: bold !important;" title="On a scale of 1 to 10, have the subject rate his or her awesomeness (if the subject answered no to question 1, select '0')">How awesome?</th>
                                    <td style="vertical-align: top;">
                                        #set($ratings = [0..10])
                                        #showBogusItems("bogus:egoMaster/awesomeness" $item $vr $ratings)
                                    </td>
                                </tr>
                                <tr>
                                    <th style="text-align: left; vertical-align: top; font-weight: bold !important;" title="What is the primary ego defense mechanism exhibited by the subject?">Ego Defense:</th>
                                    <td style="vertical-align: top;">
                                        #set($defenses = [ "Undoing", "Suppression", "Dissociation", "Idealization", "Identification", "Introjection", "Inversion", "Somatisation", "Splitting", "Substitution" ])
                                        #showBogusItems("bogus:egoMaster/defenseMechanism" $item $vr $defenses)
                                    </td>
                                </tr>
                                <tr>
                                    <th style="text-align: left; vertical-align: top; font-weight: bold !important;" title="On a scale of 1 to 10, indicate the degree of solipsism or egocentrism exhibited by the subject (enter 0 for no rating)">Solipsism:</th>
                                    <td style="vertical-align: top;">
                                        #set($solipsism = [0..10])
                                        #showBogusItems("bogus:egoMaster/solipsism" $item $vr $solipsism)
                                    </td>
                                </tr>
                                <tr>
                                    <th style="text-align: left; vertical-align: top; font-weight: bold !important;" title="On a scale of 1 to 10, indicate the degree of manifestation of anti-social personality disorder exhibited in subject behavior (enter 0 for no rating)">ASPD Score:</th>
                                    <td style="vertical-align: top;">
                                        #set($scores = [0..10])
                                        #showBogusItems("bogus:egoMaster/aspd/score" $item $vr $scores)
                                    </td>
                                </tr>
                                <tr><th style="text-align: left; vertical-align: top; font-weight: bold !important;" title="Indicate the exhibited category of ASPD via Millon's defined subtypes">ASPD Subtype:</th>
                                    <td style="vertical-align: top;">
                                        #set($subtypes = ["covetous", "defensive", "risktaking", "nomadic", "malevolent"])
                                        #set($labels = ["Covetous", "Reputation-defending", "Risk-taking", "Nomadic", "Malevolent"])
                                        #showBogusItemsAndLabels("bogus:egoMaster/aspd/subtype" $item $vr $subtypes $labels)
                                    </td>
                                </tr>
                                <tr><th style="text-align: left; vertical-align: top; font-weight: bold !important;">Notes:</th><td style="vertical-align: top;">#xdatTextArea("bogus:egoMaster/note" $item "" $vr 4 80)</td></tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top;">
                #xdatEditProps($item $edit_screen)
                <input type="button" onclick="validateForm();"  name="eventSubmit_doInsert" value="Submit"/>
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
    function validateForm() {
        var isValid = false;

        var radio = document.egoMasterForm.elements["bogus:egoMaster/awesome"];
        for (var index = 0; index < radio.length; index++) {
            if (radio[index].checked) {
                isValid = true;
                break;
            }
        }

        // TODO: Run more validation checks here.

        if (isValid) {
            document.egoMasterForm.submit();
        } else {
            alert('You must specify yes or no to the awesome question!');
        }

        return isValid;
    }
</script>
